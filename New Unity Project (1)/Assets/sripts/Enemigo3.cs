﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemigo3 : MonoBehaviour
{
    bool comprobador = false;
    public int rapidez;
    void Update()
    {
        if (transform.position.z <= -8)
        {
            comprobador = true;
        }
        if (transform.position.z >= 32)
        {
            comprobador = false;
        }
        if (comprobador)
        {
            volver();
        }
        else
        {
            ir();
        }
    }
    void ir()
    {
        transform.position += transform.forward * rapidez * Time.deltaTime;
    }
    void volver()
    {
        transform.position -= transform.forward * rapidez * Time.deltaTime;
    }


}
