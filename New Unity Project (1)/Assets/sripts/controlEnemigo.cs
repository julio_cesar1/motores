﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class controlEnemigo : MonoBehaviour
{
    GameObject player;
    public int rapidez;
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
    }
    private void Update()
    {
        transform.LookAt(player.transform);
        transform.Translate(rapidez * Vector3.forward * Time.deltaTime);
    }
   
}
