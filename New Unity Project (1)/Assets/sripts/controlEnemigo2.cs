﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;
using UnityEngine.UIElements;

public class controlEnemigo2 : MonoBehaviour
{
    GameObject player;
    public GameObject proyectil;
    public int balazo;
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        InvokeRepeating("Disparo", 1, 2);
    }
    void Update()
    {
        transform.LookAt(player.transform);
    }
    void Disparo()
    {
        GameObject pro;
        pro = Instantiate(proyectil, transform.position, Quaternion.identity);
        Rigidbody rb = pro.GetComponent<Rigidbody>();
        rb.AddForce(transform.forward*balazo,ForceMode.Impulse);
        Destroy(pro, 0.8f);
        
    }
    
}
