﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class controlEnemigo3 : MonoBehaviour
{
    public GameObject enemigo3;
    public int rapidez;
    private void recorrer()
    {
        enemigo3.transform.Translate(rapidez* Vector3.forward * Time.deltaTime);
        if(enemigo3.transform.position.z <= -8)
        {
            enemigo3.transform.position=new Vector3(0, 0.6f, 32);
        }
    }
    void Update()
    {
        recorrer();
    }
}
