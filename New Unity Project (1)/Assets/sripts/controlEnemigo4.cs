﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class controlEnemigo4 : MonoBehaviour
{ 
    GameObject player;
    public GameObject proyectil;
    public int balazo;
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        InvokeRepeating("Disparo", 1, 3);
    }
    void Update()
    {
        transform.LookAt(player.transform);
    }
    void Disparo()
    {
        GameObject pro;
        pro = Instantiate(proyectil, transform.position, Quaternion.identity);
        Rigidbody rb = pro.GetComponent<Rigidbody>();
        rb.AddForce(transform.forward*balazo,ForceMode.Impulse);
        Destroy(pro, 0.8f);

    }
    
}
