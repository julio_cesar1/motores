﻿
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class controljugador : MonoBehaviour
{
    private Rigidbody rb;
    public int rapidez;
    public LayerMask capapiso;
    public float MagnitudDeSalto;
    public SphereCollider col;
    public Text ganaste;
    public Text puntos;
    private int cont;
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        col = GetComponent<SphereCollider>();
        cont = 0;
        ganaste.text = "";
        SetearTextos();
    }

    private void SetearTextos()
    {
        puntos.text = "puntos:" + cont.ToString();
        if(cont >= 8)
        {
            ganaste.text = "Ganaste";
        }

    }

    private void FixedUpdate()
    {
        float movimientohorizontal = Input.GetAxis("Horizontal");
        float movimientovertical = Input.GetAxis("Vertical");
        Vector3 vectorMovimiento = new Vector3(movimientohorizontal, 0.0f, movimientovertical);
        rb.AddForce(vectorMovimiento * rapidez);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.R) || teCaiste())
        {
            RestarGame();
        }
        if (Input.GetKeyDown(KeyCode.Space) && EstarEnPiso())
        {
            rb.AddForce(Vector3.up * MagnitudDeSalto, ForceMode.Impulse);
        }
    }
    private bool EstarEnPiso()
    {
        return Physics.CheckCapsule(col.bounds.center, new Vector3(col.bounds.center.x, col.bounds.min.y, col.bounds.center.z), col.radius * .9f,capapiso);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Coleccionable") == true)
        {
            cont = cont + 1;
            SetearTextos();
            other.gameObject.SetActive(false);
        }
        if (other.gameObject.CompareTag("bust") == true)
        {
            rapidez +=5;
            other.gameObject.SetActive(false);
        }
        
    }
    public void RestarGame()
    {
        SceneManager.LoadScene("SampleScene");
    }
    public bool teCaiste()
    {
        return (transform.position.y <= -4);
    }

}
