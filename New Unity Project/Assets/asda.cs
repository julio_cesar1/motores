﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class asda : MonoBehaviour
{
    Personaje p1 = new Personaje("ryu", 1000);
    Personaje p2 = new Personaje("ken", 1000);
    void Start()
    {
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.L))
        {
            p1.pegar(p2);
            print(p1.Nombre + " le pego a " + p2.Nombre);
            print("a " + p2.Nombre + " le quedan " + p2.Vida);
        }
        if (Input.GetKeyDown(KeyCode.J))
        {
            p2.pegar(p1);
            print(p2.Nombre + " le pego a " + p1.Nombre);
            print("a " + p1.Nombre + " le quedan " + p1.Vida);
        }
    }
}
public class Personaje
{
    public string Nombre { get; set; }
    public int Vida { get; set; }
    public Personaje(string nom, int vid)
    {
        Nombre = nom;
        Vida = vid;
    }
    public void
    pegar(Personaje x)
    {
        x.Vida = x.Vida - 10;
    }
}
