﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class Item : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }
    Dictionary<string, int> DicItemTienda = new Dictionary<string, int>();

    //Defino una función para tener un nombre único para el diccionario.
    private string DameNombreUnico()
    {
        var caracteres = "abcdefghijklmnopqrstuvwxyz";
        var cadena = new char[8];
        var random = new System.Random();
        var finalString = new string(cadena);

        //Esto puede ser resuelto también con Linq o con recursividad.
        while (DicItemTienda.ContainsKey(finalString) || string.IsNullOrWhiteSpace(finalString))
        {
            for (int i = 0; i < cadena.Length; i++)
            {
                cadena[i] = caracteres[random.Next(caracteres.Length)];
            }

            finalString = new string(cadena);
        }
        return finalString;
    }

    //Te quedaría hacer el código en el Start(). 




    // Update is called once per frame
    void Update()
    {
        
    }
}
